package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W7d2Application {

	public static void main(String[] args) {
		SpringApplication.run(W7d2Application.class, args);
	}

}
